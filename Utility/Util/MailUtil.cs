﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Net.Mime;
using System.Windows.Forms;

namespace Utility.Util
{
    public class MailUtil
    {
        public static readonly string EMAILHOST = ConfigurationManager.AppSettings["EMAIL_HOST"];
        public static readonly string EMAILPORT = ConfigurationManager.AppSettings["EMAIL_PORT"];
        public static readonly string EMAILAUTOEMAILMSG = ConfigurationManager.AppSettings["EMAIL_AUTOEMAILMSG"];
        public static readonly string EMAILSIGNATURE = ConfigurationManager.AppSettings["EMAIL_SIGNATURE"];
        public static readonly string EMAILSYSTEMADDRESS = ConfigurationManager.AppSettings["EMAIL_SYSTEMADDRESS"];
        public static readonly string EMAILUSERNAME = ConfigurationManager.AppSettings["EMAIL_USERNAME"];
        public static readonly string EMAILPASSWORD = ConfigurationManager.AppSettings["EMAIL_PASSWORD"];

        public const string HTML_COLOR_RED = "RED";
        public const string HTML_COLOR_ORANGE = "ORANGE";
        public const string HTML_COLOR_GREEN = "GREEN";

        public static string AddHtmlColor(string input, string color)
        {
            string colorCode = "";
            if (HTML_COLOR_RED.Equals(color))
            {
                colorCode = "#FF0000";
            }
            else if (HTML_COLOR_ORANGE.Equals(color))
            {
                colorCode = "#FFA500";
            }
            else if (HTML_COLOR_GREEN.Equals(color))
            {
                colorCode = "#22B14C";
            }

            if (string.IsNullOrEmpty(colorCode))
            {
                return input;
            }
            else
            {
                return "<font color=\"" + colorCode + "\">" + input + "</font>";
            }
        }

        public static string KeyValueReportingFormat(string key, string value)
        {
            return key + "\t\t" + value;
        }

        public static string TableReportingFormat(string title, string content)
        {
            return string.Format("{0}{1}----------{1}{2}--------", title, Environment.NewLine, content);
        }

        public static void SendEmail(List<string> tos, string subject, string body)
        {
            SendEmail(EMAILSYSTEMADDRESS, EMAILSIGNATURE, tos, subject, body);
        }

        public static void SendEmail(string from, List<string> tos, string subject, string body)
        {
            SendEmail(from, from, tos, subject, body);
        }

        public static void SendEmail(string from, string fromSignature, List<string> tos, string subject, string body)
        {
            
            var mail = new MailMessage();
            //mail.From = new MailAddress("kenlin@vp.com.hk");
            mail.From = new MailAddress(from, fromSignature);
            foreach (string to in tos)
            {
                if (!string.IsNullOrEmpty(to)) mail.Bcc.Add(to);
            }
            SendMailMessage(subject, body, mail);

        }

        private static void SendMailMessage(string subject, string body, MailMessage mail)
        {
            if (string.IsNullOrEmpty(EMAILHOST))
            {
                throw new Exception("There is no Email Server in configuration file. Please contact IT HelpDesk.");
            }
            if (string.IsNullOrEmpty(EMAILPORT) || !EMAILPORT.All(char.IsDigit))
            {
                throw new Exception("There is an error on the port of Email Server in configuration file. Please contact IT HelpDesk.");
            }
            if (mail.Bcc.Count == 0)
            {
                throw new Exception("No recipient found, Report Email cannot be sent. Please contact IT HelpDesk.");
            }
            //if (string.IsNullOrEmpty(EMAILUSERNAME) || string.IsNullOrEmpty(EMAILPASSWORD))
            //{
            //    throw new Exception("SMTP Credential settings are missing. Please contact IT HelpDesk.");
            //}
            
            SmtpClient client = new SmtpClient();
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            //client.Credentials = new System.Net.NetworkCredential("EMAILUSERNAME", "EMAILPASSWORD");
            client.Host = EMAILHOST;
            client.Port = Convert.ToInt32(EMAILPORT);
            mail.Subject = subject;
            mail.Body = body;
            //mail.AlternateViews.Add(AddEmbedImage(body));
            mail.IsBodyHtml = true;
            try
            {
                client.Send(mail);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Exception occur. Please contact IT HelpDesk. Description: {0}", ex.ToString()));
            }
        }

        //private static AlternateView AddEmbedImage(string body)
        //{
        //    string seperateImage = Application.StartupPath + @"\Images\bgheader.jpg";
        //    LinkedResource inline = new LinkedResource(seperateImage);
        //    inline.ContentId = Guid.NewGuid().ToString();
        //    string htmlBody = body.Replace("seperateImage", @"cid:" + inline.ContentId);
        //    AlternateView alternateView = AlternateView.CreateAlternateViewFromString(htmlBody, null, MediaTypeNames.Text.Html);
        //    alternateView.LinkedResources.Add(inline);
        //    return alternateView;
        //}
    }
}
