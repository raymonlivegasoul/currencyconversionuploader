﻿using System;
using System.Data;
using System.Linq;

namespace Utility.Util
{
    public static class DbConvertUtil
    {
        public static int GetInt(this DataRow row, string fieldName)
        {
            return GetValue(row, fieldName, 0);
        }

        public static int GetInt(this DataRow row, string fieldName, int defaultValue)
        {
            return GetValue(row, fieldName, defaultValue);
        }

        public static decimal GetDecimal(this DataRow row, string fieldName)
        {
            return GetValue(row, fieldName, 0M);
        }

        public static decimal GetDecimal(this DataRow row, string fieldName, decimal defaultValue)
        {
            return GetValue(row, fieldName, defaultValue);
        }

        public static string GetString(this DataRow row, string fieldName)
        {
            return GetValue<string>(row, fieldName, string.Empty);
        }

        public static string GetString(this DataRow row, string fieldName, string defaultValue)
        {
            return GetValue(row, fieldName, defaultValue);
        }

        public static DateTime GetDate(this DataRow row, string fieldName, DateTime defaultValue)
        {
            return GetValue(row, fieldName, defaultValue);
        }

        public static T GetValue<T>(this DataRow row, string fieldName)
        {
            //return (row[fieldName] != System.DBNull.Value) ? (T)row[fieldName] : null;
            return GetValue<T>(row, fieldName, default(T));
        }

        public static T GetValue<T>(this DataRow row, string fieldName, T defaultValue)
        {
            return (row[fieldName] != System.DBNull.Value) ? (T)row[fieldName] : defaultValue;
        }

        public static string GetNAValue(this DataRow row, string fieldName)
        {
            var field = row[fieldName];
            if (field.GetType() is DBNull
                || field == "N/A")
            {
                return string.Empty;
            }
            else return field.ToString();
        }
    }
}
