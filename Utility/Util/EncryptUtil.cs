﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.IO;

namespace Utility.Util
{
    public class EncryptUtil
    {
        public static readonly string ENTROPYKEY = ConfigurationManager.AppSettings["ENTROPY_KEY"];
        //static string entropyKey = "MGTPassphrase";
        public static string appConnPasswordKey = "password=";

        private static byte[] GetFixedLenghtBytes(byte[] OriginalArray, int TargetLength)
        {
            byte[] targetArray = (byte[])OriginalArray.Clone();
            Array.Resize(ref targetArray, TargetLength);
            return targetArray;
        }
        public static string EncryptString(string originalData)
        {
            try
            {
                byte[] entropy = Encoding.Unicode.GetBytes(ENTROPYKEY);
                // Key must be in 32 bytes for 256bit encryption, IV must be in 16 bytes
                byte[] entropyKey = GetFixedLenghtBytes(entropy, 32);
                byte[] entropyIV = GetFixedLenghtBytes(entropy, 16);
                byte[] encryptedData = EncryptStringToBytes_Aes(originalData, entropyKey, entropyIV);
                return Convert.ToBase64String(encryptedData);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message, e.InnerException);
            }
        }

        public static string DecryptString(string encryptedData)
        {
            try
            {
                byte[] entropy = Encoding.Unicode.GetBytes(ENTROPYKEY);
                byte[] encryptedBytesWithIV = Convert.FromBase64String(encryptedData);
                // Key must be in 32 bytes for 256bit encryption, IV must be in 16 bytes
                byte[] entropyKey = GetFixedLenghtBytes(entropy, 32);

                byte[] entropyIV = new byte[16];
                byte[] encryptedBytes = new byte[encryptedBytesWithIV.Length - 16];
                // First 16 bytes is IV, the rest is CipherText
                Array.Copy(encryptedBytesWithIV, encryptedBytesWithIV.GetLowerBound(0), entropyIV, entropyIV.GetLowerBound(0), 16);
                Array.Copy(encryptedBytesWithIV, encryptedBytesWithIV.GetLowerBound(0)+16, encryptedBytes, encryptedBytes.GetLowerBound(0), encryptedBytesWithIV.Length - 16);

                string decryptedData = DecryptStringFromBytes_Aes(encryptedBytes, entropyKey, entropyIV);
                return decryptedData;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message, e.InnerException);
            }
        }

        private static byte[] EncryptStringToBytes_Aes(string plainText, byte[] Key, byte[] IV)
        {
            // Check arguments. 
            if (plainText == null || plainText.Length <= 0)
                throw new ArgumentNullException("plainText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("Key");
            byte[] encrypted;

            // Create an AesCryptoServiceProvider object with the specified key and IV. 
            using (AesCryptoServiceProvider aesAlg = new AesCryptoServiceProvider())
            {
                aesAlg.BlockSize = 128;
                aesAlg.KeySize = 256;
                aesAlg.Key = Key;
                aesAlg.IV = IV;
                aesAlg.Padding = PaddingMode.PKCS7;
                // Create a decrytor to perform the stream transform.
                ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for encryption. 
                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {
                            //Write all data to the stream.
                            swEncrypt.Write(plainText);
                        }
                        encrypted = msEncrypt.ToArray();
                    }
                }
            }

            // Append IV value to the array
            byte[] encryptedWithIV = new byte[encrypted.Length + IV.Length];
            IV.CopyTo(encryptedWithIV, 0);
            encrypted.CopyTo(encryptedWithIV, IV.Length);

            // Return the encrypted bytes from the memory stream. 
            return encryptedWithIV;

        }

        private static string DecryptStringFromBytes_Aes(byte[] cipherText, byte[] Key, byte[] IV)
        {
            // Check arguments. 
            if (cipherText == null || cipherText.Length <= 0)
                throw new ArgumentNullException("cipherText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("IV");

            // Declare the string used to hold the decrypted text. 
            string plaintext = null;

            // Create an AesCryptoServiceProvider object with the specified key and IV. 
            using (AesCryptoServiceProvider aesAlg = new AesCryptoServiceProvider())
            {
                aesAlg.BlockSize = 128;
                aesAlg.KeySize = 256;
                aesAlg.Key = Key;
                aesAlg.IV = IV;
                aesAlg.Padding = PaddingMode.PKCS7;
                // Create a decrytor to perform the stream transform.
                ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for decryption. 
                using (MemoryStream msDecrypt = new MemoryStream(cipherText))
                {
                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                        {
                            // Read the decrypted bytes from the decrypting stream and place them in a string.
                            plaintext = srDecrypt.ReadToEnd();
                        }
                    }
                }
            }
            return plaintext;

        }

    }
}
