﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utility.LogEngine
{
    public class Logger
    {
        #region Properties
        #endregion

        #region Constructors
        public Logger()
        {
        }
        #endregion

        #region Methods
        public void Write(string message)
        {
            Console.WriteLine(message);
        }

        public void Write(string message, params object[] args)
        {
            if ((args == null) || (args.Length == 0)) Console.WriteLine(message);
            Console.WriteLine(message, args);
        }

        public void WriteException(Exception ex)
        {
            Write(ex.Message + "\r\n" + ex.StackTrace);
        }

        public void WriteException(string ExceptionMessage)
        {
            Write(ExceptionMessage);
        }

        public void RedirectConsoleOutput(EventHandler<ConsoleWriterEventArgs> handler)
        {
            using (var consoleWriter = new ConsoleWriter())
            {
                consoleWriter.WriteEvent += handler;
                consoleWriter.WriteLineEvent += handler;

                Console.SetOut(consoleWriter);
            }
        }
        #endregion
    }
}
