﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility.Util;

namespace Utility.LogEngine
{
    [Serializable]
    public class LogModel
    {
        #region Properties
        public string UserId { get; set; }
        public LogPriorityEnum Priority { get; set; }
        public LogTypeEnum Type { get; set; }
        public DateTime UpdateTime { get; set; }
        public string Message { get; set; }
        public string ServerName { get; set; }
        #endregion

        #region Constructors
        public LogModel()
        {
            UpdateTime = DateTime.Now;
            Type = LogTypeEnum.General;
            Priority = LogPriorityEnum.Info;
            UserId = string.Empty;
        }

        public LogModel(string message)
            : this()
        {
            Message = message;
        }
        #endregion

        #region Methods
        public virtual void Load(System.Data.DataRow row)
        {
            UserId = (string)row["UserId"];
            Priority = ConvertUtil.EnumParse<LogPriorityEnum>(row["Priority"]);
            Type = ConvertUtil.EnumParse<LogTypeEnum>(row["Type"]);
            UpdateTime = (DateTime)row["UpdateTime"];
            Message = (string)row["Message"];
            ServerName = (string)row["ServerName"];
        }
        #endregion
    }
}
