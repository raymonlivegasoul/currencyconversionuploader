﻿using System;
using System.Configuration;
using System.Linq;
using Utility.Database;

namespace Utility.LogEngine
{
    public class LogDal
    {
        private static readonly string defaultDb = ConfigurationManager.AppSettings["Log_Database"];
        private static DBManager manager = DBManager.GetInstance();

        private static IDatabase GetDB()
        {
            return manager.GetDatabase(defaultDb);
        }

        public static void LogUpdate(LogModel log)
        {
            var db = GetDB();
            db.RunQuery("INSERT INTO [AppLog] (UserId, Priority, Type, UpdateTime, Message) VALUES (@USERID, @Priority, @Type, @UpdateTime, @Message)");
            db.AddParameter("UserId", log.UserId);
            db.AddParameter("Priority", log.Priority.ToString());
            db.AddParameter("Type", log.Type.ToString());
            db.AddParameter("UpdateTime", log.UpdateTime);
            db.AddParameter("Message", log.Message);
            db.ExecuteNonQuery();
        }
    }
}
