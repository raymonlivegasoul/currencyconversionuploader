﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utility.LogEngine
{
    public enum LogPriorityEnum
    {
        Fatal = 0,
        Error = 1,
        Warning = 2,
        Info = 3,
        Trace = 4,
        Debug = 5,
    }

    public enum LogTypeEnum
    {
        General = 0,
        Server = 1,
        Client = 2,
        Task = 3,
        Security = 4,
        Agent = 5,
    }
}
