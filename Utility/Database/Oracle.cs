﻿using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Data.Oracle;
using EnterpriseLibrary = Microsoft.Practices.EnterpriseLibrary.Data.Oracle;

namespace DBManager
{
    public class Oracle : IDatabase
    {
        private readonly EnterpriseLibrary.OracleDatabase database;
        private DbCommand command;

        public Oracle(string connectionString)
        {
            database = new OracleDatabase(connectionString);
        }

        public void RunQuery(string queryString)
        {
            command = database.GetSqlStringCommand(queryString);
        }

        public void RunStoredProc(string commandName)
        {
            command = database.GetStoredProcCommand(commandName);
        }

        public void AddParameter(string parameterName, object parameter)
        {
            AddParameter(parameterName,  GetDBType(parameter), parameter);
        }

        public void AddParameter(string parameterName,  DbType type, object parameter)
        {
            if (parameter is Enum)
            {
                parameter = Convert.ToInt32(parameter);
            }
            database.AddInParameter(command, parameterName, type, parameter);
        }


        public void AddOutParameter(string parameterName, DbType type)
        {
            database.AddOutParameter(command, parameterName, type, 0);
            
        }

        public object GetValue(string parameterName)
        {
            return database.GetParameterValue(command, parameterName);
        }

        private static DbType GetDBType(object parameter)
        {
            DbType result = DbType.String;
            if (parameter is String)
            {
                result = DbType.String;
            }
            else if (parameter is Guid)
            {
                result = DbType.Guid;
            }
            else if (parameter is int)
            {
                result = DbType.Int32;
            }
            else if (parameter is double)
            {
                result = DbType.VarNumeric;
            }
            else if (parameter is bool)
            {
                result = DbType.Boolean;
            }
            else if (parameter is DateTime)
            {
                result = DbType.DateTime;
            }
            else if (parameter is Enum)
            {
                result = DbType.Int32;
            }

            return result;
        }

        #region Execute command

        public DataTable ExecuteDataTable()
        {
            DataTable table = new DataTable();
            DataSet set = ExecuteDataSet();
            if (set.Tables.Count > 0)
            {
                table = set.Tables[0];
            }
            return table;
        }

        public DataSet ExecuteDataSet()
        {
            return database.ExecuteDataSet(command);
        }

        public void ExecuteNonQuery()
        {
            database.ExecuteNonQuery(command);
        }

        public object ExecuteScalar()
        {
            return database.ExecuteScalar(command);
        }

        public string ExecuteString()
        {
            return (string) database.ExecuteScalar(command);
        }
        #endregion
    }
}

