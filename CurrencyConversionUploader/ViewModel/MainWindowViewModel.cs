﻿using Renci.SshNet;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows;
using Utility.Database;
using Utility.LogEngine;
using Utility.Util;

namespace CurrencyConversionUploader.ViewModel
{
    public class MainWindowViewModel : BaseViewModel
    {
        #region Properties
        private const string _UPLOAD_FILE_TEMPLATE = "{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13}";
        private readonly string UPLOAD_PATH = ConfigUtil.GetAppSettings("UPLOAD_PATH");
        private readonly string UPLOAD_PATH_BACKUP = ConfigUtil.GetAppSettings("UPLOAD_PATH_BACKUP");
        private readonly string FTP_SERVER_PATH = ConfigUtil.GetAppSettings("FTP_SERVER_PATH");
        private readonly string FTP_ACCOUNT = ConfigUtil.GetAppSettings("FTP_ACCOUNT");
        private readonly string FTP_PASSWORD = ConfigUtil.GetAppSettings("FTP_PASSWORD");
        public RangeObservableCollection<FxModel> FxList { get; set; }

        public Dictionary<string, string> AccountMapper { get; set; }

        private DateTime _selectedDate;
        public DateTime SelectedDate
        {
            get { return _selectedDate; }
            set
            {
                if (_selectedDate == value) return;
                _selectedDate = value;
                OnPropertyChanged("EndDate");
                LoadFromDB();
            }
        }

        private ObservableCollection<LogModel> _messages = new ObservableCollection<LogModel>();
        public ObservableCollection<LogModel> Messages
        {
            get
            {
                return _messages;
            }
        }
        #endregion

        #region Constructors
        public MainWindowViewModel()
        {
            Current.Logger = new Logger();
            Current.Logger.RedirectConsoleOutput(ConsoleWriter_WriteEvent);

            FxList = new RangeObservableCollection<FxModel>();
            LoadAccountMapping();
            SelectedDate = DateTime.Now.AddHours(-12).Date;
        }
        #endregion

        #region Methods
        public async void LoadFromDB()
        {
            await DoAsync(() =>
            {
                var list = new List<FxModel>();
                var db = DBManager.ConfigManager;
                db.RunQuery(@"
SELECT  * 
FROM    FxConversion
where   TradeDate = @tradeDate
ORDER BY TimeStamp
");
                db.AddParameter("tradeDate", SelectedDate);
                var tb = db.ExecuteDataTable();
                foreach (DataRow row in tb.Rows)
                {
                    var fx = new FxModel();
                    fx.Load(row);
                    list.Add(fx);
                }

                FxList.Clear();
                FxList.AddRange(list);

                Console.Write(string.Format("FX conversion as of date {0} is loaded from DB.", SelectedDate.ToString("yyyy/MM/dd")));
            });
        }

        public void UploadFile()
        {
            var filePath = BuildUploadFile();
            if (string.IsNullOrEmpty(filePath))
            {
                Console.Write("Upload File Failed");
                return;
            }
            //UploadFtp(filePath);
            UploadFileSFTP(filePath);
            //BackupFile();
            
        }

        public string BuildUploadFile()
        {
            //var selectedList = FxList.Where(value => value.IsSelected).ToList();
            //if (selectedList.Count == 0)
            //{
            //    MessageBox.Show("Please select trades to upload.");
            //    return;
            //}
            //if (selectedList.Any(value => string.IsNullOrEmpty(value.TransactionId)))
            //{
            //    MessageBox.Show("Please input valid Transaction ID.");
            //    return;
            //}

            var selectedList = FxList.Where(value => !string.IsNullOrEmpty(value.TransactionId)).ToList();
            if (selectedList.Count == 0)
            {
                MessageBox.Show("There are no valid trades to upload.");
                return string.Empty; ;
            }

            var sb = new StringBuilder();
            //sb.AppendLine(string.Format("Transaction, Status, Trade Date, Type, Acct #,Buy/Sell,Ccy,Amount,x ccy,Rate,Value,Broker code"));
            sb.AppendLine(string.Format("Trade ID, Status, Trade Date, Trade Type, Account, Buy/Sell, Traded Currency, Amount, Contra Currency, Rate, Value Date, Executing Broker, NDF, PrimeBroker"));
            //Trade ID	Status	Trade Date	Trade Type	Account	Buy/Sell	Traded Currency	Amount	Contra Currency	Rate	Value Date	Executing Broker	NDF	PrimeBroker
            //http://fxtrades.currenex.net/webstart/requester
            foreach (var fx in selectedList)
            {
                var account = fx.Account;
                if (AccountMapper.ContainsKey(account))
                {
                    account = AccountMapper[account];
                }
                sb.AppendLine(string.Format(_UPLOAD_FILE_TEMPLATE, fx.TransactionId, "New", fx.TradeDate.ToString("d/M/yyyy"), "SP", account, fx.BuySellString,
                    fx.Currency, fx.Amount, "USD", fx.Rate, fx.ValueDate.ToString("d/M/yyyy"), "Currenex", "", "Newedge"));
            }
            var path = @"Upload\";
            var filePath = string.Format("{0}Vegasoul_S-{1}.csv", path, DateTime.Now.ToString("yyyyMMddHHmmss"));
            Directory.CreateDirectory(path);
            if (File.Exists(filePath))
            {
                Console.Write("Export failed, please try again.");
                return string.Empty;
            }
            File.WriteAllText(filePath, sb.ToString());
            Console.Write("Trade file is exported to the path " + filePath);
            return filePath;
        }

        public void UploadFtp(string filePath)
        {
            var fileInfo = new FileInfo(filePath);
            if (fileInfo.Extension.ToLower() != ".csv") return;
            var request = (FtpWebRequest)WebRequest.Create(FTP_SERVER_PATH + fileInfo.Name);
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(FTP_ACCOUNT, FTP_PASSWORD);

            var sourceStream = new StreamReader(filePath);
            byte[] fileContents = Encoding.UTF8.GetBytes(sourceStream.ReadToEnd());
            sourceStream.Close();
            request.ContentLength = fileContents.Length;

            Stream requestStream = request.GetRequestStream();
            requestStream.Write(fileContents, 0, fileContents.Length);
            requestStream.Close();

            FtpWebResponse response = (FtpWebResponse)request.GetResponse();

            Console.Write("{0} Upload Complete, status {1}", fileInfo.Name, response.StatusDescription);

            response.Close();

            BackupFile(filePath);
        }

        public FtpWebResponse UploadFTPFile(FileInfo fileInfo)
        {
            var request = (FtpWebRequest)WebRequest.Create(FTP_SERVER_PATH + fileInfo.Name);
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(FTP_ACCOUNT, FTP_PASSWORD);
            request.EnableSsl = true;

            var sourceStream = new StreamReader(fileInfo.FullName);
            byte[] fileContents = Encoding.UTF8.GetBytes(sourceStream.ReadToEnd());
            sourceStream.Close();
            request.ContentLength = fileContents.Length;

            Stream requestStream = request.GetRequestStream();
            requestStream.Write(fileContents, 0, fileContents.Length);
            requestStream.Close();

            FtpWebResponse response = (FtpWebResponse)request.GetResponse();
            return response;
        }

        public void UploadFileSFTP(string filePath)
        {
            using (var client = new SftpClient(FTP_SERVER_PATH, FTP_ACCOUNT, FTP_PASSWORD))
            {
                client.Connect();
                if (client.IsConnected)
                {
                    Console.Write("SFTP connection succeeded");

                    var fileInfo = new FileInfo(filePath);
                    if (fileInfo.Extension.ToLower() != ".csv") return;
                    var sourceStream = new StreamReader(fileInfo.FullName);
                    byte[] csvFile = Encoding.UTF8.GetBytes(sourceStream.ReadToEnd());
                    sourceStream.Close();

                    using (var ms = new MemoryStream(csvFile))
                    {
                        client.BufferSize = (uint)ms.Length; // bypass Payload error large files
                        client.UploadFile(ms, "/Incoming/" + fileInfo.Name);
                        BackupFile(filePath);
                        Console.Write("Upload File Completed");
                    }
                }
                else
                {
                    Console.Write("SFTP connection failed");
                }
            }
        }

        void BackupFile(string file)
        {
            Directory.CreateDirectory(UPLOAD_PATH_BACKUP);
            var targetFile = file.Replace(UPLOAD_PATH, UPLOAD_PATH_BACKUP);
            if (File.Exists(targetFile))
            {
                File.Delete(targetFile);
            }
            File.Move(file, targetFile);
        }

        void ConsoleWriter_WriteEvent(object sender, ConsoleWriterEventArgs e)
        {
            var log = new LogModel(e.Value);
            log.Type = LogTypeEnum.Client;
            log.UserId = Environment.UserName;
            WriteMessage(log);
        }

        public void WriteMessage(LogModel message)
        {
            Util.DispatcherInvoke(() =>
                    Messages.Add(message)
                );
        }

        private void process_OutputDateReceived(object sender, DataReceivedEventArgs e)
        {
            if (string.IsNullOrEmpty(e.Data)) return;
            Console.WriteLine(e.Data);
        }

        private void LoadAccountMapping()
        {
            AccountMapper = new Dictionary<string, string>();
            var lines = File.ReadAllLines("AccountMapping.txt");
            foreach (var line in lines)
            {
                var array = line.Split(',');
                AccountMapper.Add(array[0].Trim(), array[1].Trim());
            }
        }
        #endregion
    }

    public class FxModel
    {
        #region Properties
        public DateTime TradeDate { get; set; }
        public string Account { get; set; }
        public string Currency { get; set; }
        public int BuySell { get; set; }
        public double Amount { get; set; }
        public double Rate { get; set; }
        public DateTime ValueDate { get; set; }
        public DateTime TimeStamp { get; set; }
        public string TransactionId { get; set; }
        public bool IsSelected { get; set; }
        public string Message { get; set; }

        public string BuySellString
        {
            get { return BuySell == 1 ? "B" : "S"; }
        }
        #endregion

        #region Constructor
        #endregion

        #region Methods
        public void Load(DataRow row)
        {
            TradeDate = (DateTime)row["TradeDate"];
            Account = (string)row["Account"];
            Currency = (string)row["Currency"];
            BuySell = (int)row["BuySell"];
            Amount = (double)row["Amount"];
            Rate = (double)row["Rate"];
            ValueDate = (DateTime)row["ValueDate"];
            TimeStamp = (DateTime)row["TimeStamp"];
        }
        #endregion
    }
}
