﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace CurrencyConversionUploader.ViewModel
{

    public abstract class BaseViewModel : INotifyPropertyChanged
    {
        #region Properties

        private bool _isBusy;
        public bool IsBusy
        {
            get
            {
                return _isBusy;
            }
            set
            {
                _isBusy = value;
                OnPropertyChanged("IsBusy");
                OnPropertyChanged("NotBusy");
            }
        }

        public bool NotBusy
        {
            get { return !IsBusy; }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Do job in async mode
        /// </summary>
        public async Task DoAsync(Action action)
        {
            IsBusy = true;
            await Task.Run(() =>
            {
                try
                {
                    action();
                }
                catch (AggregateException ae)
                {
                    foreach (var ex in ae.InnerExceptions)
                    {
                        throw;
                        //Current.Logger.WriteException(ex);
                    }
                }
                catch (Exception ex)
                {
                    throw;
                   // Current.Logger.WriteException(ex);
                }
                finally { IsBusy = false; }
            });
        }

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
        #endregion
    }
}
