﻿using System;

namespace CurrencyConversionUploader
{
    public class Util
    {
        public static void DispatcherInvoke(Action action)
        {
            Current.MainWindow.Dispatcher.BeginInvoke(action);
        }
    }
}
