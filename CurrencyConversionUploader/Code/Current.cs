﻿using System;
using System.Windows;
using Utility.LogEngine;

namespace CurrencyConversionUploader
{
    public class Current
    {
        public static Logger Logger { get; set; }
        public static Window MainWindow { get; set; }
    }
}
