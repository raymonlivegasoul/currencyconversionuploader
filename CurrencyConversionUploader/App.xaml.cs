﻿using System;
using System.Windows;
using System.Windows.Threading;

namespace CurrencyConversionUploader
{
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            Application.Current.DispatcherUnhandledException +=
              new DispatcherUnhandledExceptionEventHandler(DispatcherUnhandledExceptionHandler);

            base.OnStartup(e);
        }

        void Current_Exit(object sender, ExitEventArgs e)
        {
        }

        private void DispatcherUnhandledExceptionHandler(object sender, DispatcherUnhandledExceptionEventArgs args)
        {
            try
            {
                var ex = (args.Exception.InnerException == null)
                    ? args.Exception : args.Exception.InnerException;
                MessageBox.Show(ex.Message + Environment.NewLine + ex.StackTrace);
                args.Handled = true;
            }
            catch { }
        }
    }
}