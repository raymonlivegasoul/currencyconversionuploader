﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using CurrencyConversionUploader.ViewModel;
using Telerik.Windows.Controls;

namespace CurrencyConversionUploader
{
    public partial class MainWindow : Window
    {
        public MainWindowViewModel ViewModel
        {
            get { return DataContext as MainWindowViewModel; }
        }
        public MainWindow()
        {
            Current.MainWindow = this;
            DataContext = new MainWindowViewModel();
            InitializeComponent();
        }

        private void btnRefresh_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.LoadFromDB();
        }

        private void btnExportTrades_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.BuildUploadFile();
        }

        private void btnUploadTrades_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.UploadFile();
        }

        private void RadGridView_PastingCellClipboardContent(object sender, GridViewCellClipboardEventArgs e)
        {
            if (e.Value == null) return;
            if (e.Value.Equals("Trade ID")) return;
            if (e.Cell.Column.UniqueName == "TransactionId")
            {
                var content = e.Value.ToString();
                if (string.IsNullOrEmpty(content)) return;
                var lines = e.Value.ToString().Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
                var line = lines[lines.Count() - 1];
                var fields = line.Split('\t');
                e.Value = fields[0];
            }
        }
    }
}
